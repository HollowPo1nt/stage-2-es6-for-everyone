import { controls } from '../../constants/controls';
import { waitFor } from '@babel/core/lib/gensync-utils/async';

const controller = {
  'KeyA': { pressed: false },
  'KeyD': { pressed: false },
  'KeyJ': { pressed: false },
  'KeyL': { pressed: false },
  'KeyQ': { pressed: false },
  'KeyW': { pressed: false },
  'KeyE': { pressed: false },
  'KeyU': { pressed: false },
  'KeyI': { pressed: false },
  'KeyO': { pressed: false }
};

export async function fight(firstFighter, secondFighter) {
  let winner = null;

  document.addEventListener('keydown', (e) => {
    if (controller[e.code]) {
      controller[e.code].pressed = true;
    }

    if (controller[controls.PlayerOneAttack].pressed && !controller[controls.PlayerOneBlock].pressed && !controller[controls.PlayerTwoBlock].pressed) {
      secondFighter.health -= getDamage(firstFighter, secondFighter);
      // document.getElementById('health-bar')
    }

    if (controller[controls.PlayerTwoAttack].pressed && !controller[controls.PlayerTwoBlock].pressed && !controller[controls.PlayerOneBlock].pressed) {
      firstFighter.health -= getDamage(secondFighter, firstFighter);
    }

    // if (controller.controls.PlayerOneCriticalHitCombination){
    //
    // }
    console.log(firstFighter.health + ':' + secondFighter.health);

    if (firstFighter.health <= 0) {
      return Promise.resolve(secondFighter);
    }

    if (secondFighter.health <= 0) {
      return Promise.resolve(firstFighter);
    }

    winner = getWinner(firstFighter, secondFighter);
  });
  document.addEventListener('keyup', (e) => {
    if (controller[e.code]) {
      controller[e.code].pressed = false;
    }
  });

  return new Promise((resolve, reject) => {
    setTimeout(() => (!getWinner(firstFighter, secondFighter) ? resolve(getWinner(firstFighter, secondFighter)) : winner), 500);
  });;
}

async function getWinner(firstFighter, secondFighter) {
  if (firstFighter.health <= 0) {
    return Promise.resolve(secondFighter);
  } else if (secondFighter.health <= 0) {
    return Promise.resolve(firstFighter);
  } else {
    return null;
  }
}

function fighterDefeated(fighter) {
  return fighter;
}

export function getDamage(attacker, defender) { // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);

  if (damage < 0) {
    damage = 0;
  }

  return damage;
}

export function getHitPower(fighter) { // return hit power
  return fighter.attack * getCriticalHitChance();
}

function getCriticalHitChance() {
  return randomInteger(1, 2);
}

export function getBlockPower(fighter) { // return block power
  return fighter.defense * getDogeChance();
}

function getDogeChance() {
  return randomInteger(1, 2);
}

function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}