import { createElement } from '../helpers/domHelper';
import { showModal } from './modal/modal';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    attributes: {
      image: createFighterImage(fighter),
      name: fighter.name,
      health: fighter.health,
      attack: fighter.attack,
      defence: fighter.defence
    }
  });

  // todo: show fighter info (image, name, health, etc.)

  showModal({title: fighter.name,
  bodyElement:{
    image: createFighterImage(fighter),
    health: fighter.health,
    attack: fighter.attack,
    defence: fighter.defence
  }});
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-preview___img', attributes });

  return imgElement;
}
