import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({ title: 'Winner is ' + fighter.name, bodyElement: 'remained health: ' + fighter.health });
}
